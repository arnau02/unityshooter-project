using System;
using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEditor;
using UnityEngine;
using UnityEngine.Serialization;
using UnityEngine.SceneManagement;

public class PlayerController : MonoBehaviour, IDamageable
{
    private readonly Vector3 gravity=Physics.gravity;
        [Header("Estadistiques del personatge")]
        [Space]
        [SerializeField]
        private float speed = 3f;
        [SerializeField]
        private float mouseSensitivity = 1f;
        [SerializeField] 
        private float hp;
        [SerializeField]
        private int dmg;

        [SerializeField] private int dmgarma2 = 3;
        [SerializeField]
        private float jumpHeight;

        private float acumuladorDisper;

        [SerializeField] private float shoot1Range = 35f;
        [SerializeField] private float shoot2Range = 15f;
        
        private Vector3 cameraRotation;
        Vector3 movement = Vector3.zero;
        private Rigidbody rigidBody;
        private int shootMode=1;

        private bool canShoot = true;
       
        
        private bool peakPoint;
      
        //Camera
        [Space]
        [Header("Coses del personatge")]
        [Space]
        [SerializeField]
        private GameObject camera;
       
        //Enemy Interaction
        [SerializeField]
        private LayerMask interactionMask;
        
        private CapsuleCollider colliderCharacter;
        private BoxCollider colliderBox;
        

        private void Awake()
        {
            Cursor.lockState = CursorLockMode.Locked;
            rigidBody = GetComponent<Rigidbody>();
            colliderCharacter = GetComponent<CapsuleCollider>();
            colliderBox = GetComponent<BoxCollider>();
            

        }

        void Update()
        {
           
            /************************************************************CAMERA REGION************************************************************/

            #region CAMERA
            cameraRotation = new Vector3(Mathf.Clamp(Input.GetAxis("Mouse Y")*mouseSensitivity+acumuladorDisper,-90,90) , 0, 0);
            transform.eulerAngles = transform.eulerAngles- new Vector3(0, Input.GetAxis("Mouse X") * -mouseSensitivity, 0);
            camera.transform.eulerAngles = camera.transform.eulerAngles - cameraRotation;
            //
            #endregion
            
            
            /************************************************************MOVEMENT REGION************************************************************/
            #region MOVEMENT
            movement = Vector3.zero;

            if (Input.GetKey(KeyCode.W))
                movement += transform.forward;
            if (Input.GetKey(KeyCode.S))
                movement -= transform.forward;

            //strafe
            if (Input.GetKey(KeyCode.A))
                movement -= transform.right;
            if (Input.GetKey(KeyCode.D))
                movement += transform.right;
            
            movement.Normalize();
            movement *= speed;
            

            #endregion
           
            /************************************************************JUMP REGION************************************************************/
            #region JUMP

            
            
            if (Input.GetKeyDown(KeyCode.Space) && isGroundedSphere())
            {
                rigidBody.AddForce(0,jumpHeight,0, ForceMode.Impulse);
                

            }

            
            
            if (rigidBody.velocity.y<0&&!peakPoint)
            {
                Physics.gravity = new Vector3(Physics.gravity.x, Physics.gravity.y+(Physics.gravity.y * 100) / 100, Physics.gravity.z);
                peakPoint = true;
            }

            if (isGroundedSphere())
            {
                Physics.gravity = gravity;
                peakPoint = false;

            }
                        
            
            //Debug.Log(Physics.gravity);
            

            #endregion
            /************************************************************SHOOT REGION************************************************************/
            #region SHOOT

            if (Input.GetKeyDown(KeyCode.Alpha1))
            {
                shootMode = 1;
                GameManager.Instance.SetDebugText("Shoot 1 selected");
            }

            if (Input.GetKeyDown(KeyCode.Alpha2))
            {
                shootMode = 2;
                GameManager.Instance.SetDebugText("Shoot 2 selected");
            }
            
            if (Input.GetMouseButtonDown(0)&&shootMode==1)
            {
                if (canShoot)
                {
                    Shoot(); 
                }
                
            }else if (Input.GetMouseButton(0)&&shootMode==2)
            {
                if (canShoot)
                {
                    Shoot();
                }
            }

            if (Input.GetMouseButtonUp(0))
            {
                acumuladorDisper = 0;
                canShoot = true;
                StopAllCoroutines();
            }

            #endregion
              
        }

        private void FixedUpdate()
        {
            //rigidBody.MovePosition(transform.position + movement.normalized * speed * Time.fixedDeltaTime);
            rigidBody.velocity = movement.normalized * speed + Vector3.up * rigidBody.velocity.y;
        }

        private void Shoot()
        {
            canShoot = false;
            RaycastHit hit;
            if (shootMode == 1)
            {
                if (Physics.Raycast(camera.transform.position, camera.transform.forward, out hit, shoot1Range))
                {
                    //Debug.Log($"He tocat {hit.collider.gameObject} a la posici {hit.point} amb normal {hit.normal}");
                    Debug.DrawLine(camera.transform.position, hit.point, Color.blue, 2f);
                

                    if (hit.collider.TryGetComponent<IDamageable>(out IDamageable target))
                    {
                        if (hit.collider.TryGetComponent<HitboxController>(out HitboxController hitbox))
                        {
                            hitbox.GetMultiplier(dmg); 
                        }
                   
                    }
                    if (hit.collider.TryGetComponent<IPushable>(out IPushable pushable))
                        pushable.Push(camera.transform.forward, 10);
                    //Gestionar en classe.
                    //hit.point
                    StartCoroutine(shootCooldown());
                }
            }
            else
            {
              if(Physics.Raycast(camera.transform.position, camera.transform.forward, out hit, shoot2Range))
              {
                  Debug.DrawLine(camera.transform.position, hit.point, Color.blue, 2f);
                  if (hit.collider.TryGetComponent<IDamageable>(out IDamageable target))
                  {
                      if (hit.collider.TryGetComponent<HitboxController>(out HitboxController hitbox))
                      {
                          hitbox.GetMultiplier(dmgarma2); 
                      }
                   
                  }

                  if (hit.collider.TryGetComponent<IPushable>(out IPushable pushable))
                  {
                      pushable.Push(camera.transform.forward, 10); 
                  }
                      
                  
                  StartCoroutine(shootCooldownAuto());
              }
            }
            
        }
        
        private bool isGrounded()
        {
            Vector3 origin = transform.position;
            return Physics.Raycast(origin, Vector3.down, colliderCharacter.height+0.5f, interactionMask);
            
            
          
        }

        private bool isGroundedSphere()
        {
            //Vector3 origin = new Vector3(transform.position.x,colliderCharacter.bounds.min.y,transform.position.z);
            Vector3 origin = new Vector3(transform.position.x,colliderBox.bounds.min.y,transform.position.z);
            return Physics.CheckSphere(origin, colliderCharacter.radius, interactionMask);
            //return Physics.SphereCast(rayOrigin,colliderCharacter.radius * 1.2f,1f,interactionMask);

        }
        
        public void LooseHP(float dmg)
        {
            hp -= dmg;
            GameManager.Instance.SetTextVida(hp.ToString());
            if (hp <= 0)
            {
                SceneManager.LoadScene("DeathScene");
            }
        }

        IEnumerator shootCooldown()
        {
            yield return new WaitForSeconds(2f);
            canShoot = true;
        }
        
        IEnumerator shootCooldownAuto()
        {
            
            yield return new WaitForSeconds(0.5f);
            canShoot = true;
            acumuladorDisper = 0.3f;
        }

        private void Start()
        {
            GameManager.Instance.SetTextVida(hp.ToString());
        }
}
