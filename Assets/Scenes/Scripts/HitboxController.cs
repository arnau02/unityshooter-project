using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HitboxController : MonoBehaviour, IPushable, IDamageable
{
    [SerializeField]
    private EnemyController enemy;
    public float multiplier;
    public void GetMultiplier(float damage)
    {
        GameManager.Instance.SetDebugText($"{gameObject.name} impactat per un  multiplicador de  {multiplier}. ");
        LooseHP(damage*multiplier);
        
    }

    public void LooseHP(float damage)
    {
        enemy.LooseHP(damage);
    }

    public void Push(Vector3 direction, float impulse)
    {
        GetComponent<Rigidbody>().AddForce(direction * impulse, ForceMode.Impulse);
    }
}
