using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor.Animations;

[CreateAssetMenu(fileName = "StatsEnemic", menuName = "Scriptable Objects/EnemyStats")]
public class EnemySo : ScriptableObject
{
    [Header("Gameplay Attributes")]
    [SerializeField]
    [Range(0,30)]
    [Tooltip("This will determine the Health Points.")]
    [Space]
    private float m_hp;
    public float hp => m_hp;

    [SerializeField]
    [Range(0, 10)]
    [Tooltip("This will determine the Damage that the enemy does.")]
    [Space]
    private int m_dmg;
    public int dmg => m_dmg;

    [SerializeField]
    [Range(0, 8)]
    [Tooltip("This will determine the enemy Movement Speed.")]
    [Space]
    private int m_speed;
    public int speed => m_speed;


    [Header("NonVisual Atributes")]
    [SerializeField]
    [Range(0, 25)]
    [Tooltip("This is the range that the enemy will be able to see the player.")]
    [Space]
    private int m_detection_range;
    public int detection_range => m_detection_range;

    [SerializeField]
    [Range(0, 20)]
    [Tooltip("This will determine the range that the enemy needs to be close to the player to attack him.")]
    [Space]
    private int m_attack_range;
    public int attack_range => m_attack_range;

    [SerializeField]
    [Range(0, 8)]
    [Tooltip("This will determine the proyectile speed(In case it has one).")]
    [Space]
    private int m_proyectile_speed;
    public int proyectile_speed => m_proyectile_speed;

    [SerializeField]
    [Tooltip("This will determine Patrol Points the enemy can use, in case is a Patrol Enemy.")]
    [Space]
    private Vector3[] m_patrol_points_list;
    public Vector3[] patrol_points_list => m_patrol_points_list;

    [SerializeField]
    [Tooltip("This will determine the animation the enemy can use.")]
    [Space]
    private AnimatorController m_animator;
    public AnimatorController animator => m_animator;

    [SerializeField]
    [Tooltip("This will determine the color from the enemy.")]
    [Space]
    private Color m_color;
    public Color colorsito => m_color;


}


