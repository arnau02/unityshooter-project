using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.AI;
using UnityEngine.Serialization;

public class EnemyController : MonoBehaviour,IDamageable
{

    [HideInInspector] public EnemySo stats;

    [Header("Parametres")] [SerializeField]
    public EnemySo[] listaSo;

    [SerializeField] private LayerMask playerHurtbox;
    [SerializeField] private LayerMask playerPushBox;

    /// <summary>
    /// Paràmetres per a les característiques del enemic
    /// </summary>
    private float hp;

    private int dmg;
    private Animator animator;
    private float speed;
    private int detectionRange;
    private int attackRange;
    
    [SerializeField]
    private GameObject camera;
    
    private NavMeshAgent meshAgent;
    private NavMeshPath meshPath;

    /// <summary>
    /// Paràmetres per a la lògica del enemic
    /// </summary>
    private bool detect = false;

    private Vector3 target;
    [HideInInspector]
    public Vector3 patrolTarget;
    private float distance;

    private Vector3[] patrolPoints { get; set; }

    private bool canShoot = true;

    private bool cameraMode = false;

    private Vector3 cameraRotation;

    [SerializeField]
    private GameObject m_RigRoot;
    private Rigidbody[] m_Bones;
    private bool dead=false;

    private enum SwitchMachineStates
    {
        NONE,
        IDLE,
        CHASE,
        ATTACK,
        PATROL,
        DEAD
    };
    [SerializeField]
    private SwitchMachineStates m_CurrentState;
    private SwitchMachineStates oldState;

    //canviarem d'estat sempre mitjanant aquesta funci
    private void ChangeState(SwitchMachineStates newState)
    {

        if (newState == m_CurrentState)
            return;

        ExitState();
        InitState(newState);
    }

    private void InitState(SwitchMachineStates currentState)
    {
        
        //m_CurrentState = currentState;
        switch (currentState)
        {
            case SwitchMachineStates.IDLE:
                if (m_CurrentState == SwitchMachineStates.DEAD)
                {
                    break;
                }

                m_CurrentState = currentState;
                

                meshAgent.speed = 0;



                break;

            case SwitchMachineStates.CHASE:
                if (m_CurrentState == SwitchMachineStates.DEAD)
                {
                    break;
                }
                m_CurrentState = currentState;
                meshAgent.SetDestination(target);
                meshAgent.speed = speed;
                
                //Debug.Log(m_orientation);



                break;

            case SwitchMachineStates.ATTACK:
                if (m_CurrentState == SwitchMachineStates.DEAD)
                {
                    break;
                }
                m_CurrentState = currentState;
                // Debug.Log("Ataco");
                meshAgent.speed = 0;


                break;

            case SwitchMachineStates.PATROL:
                if (m_CurrentState == SwitchMachineStates.DEAD)
                {
                    break;
                }
                m_CurrentState = currentState;
                Vector3 randomPosition;
                int randomx = Random.Range(-58, 58);
                int randomz = Random.Range(-58, 58);
                randomPosition = new Vector3(randomx, 1, randomz);
                NavMeshHit meshHit;
                NavMesh.SamplePosition(randomPosition, out meshHit,1f, NavMesh.AllAreas);
                meshAgent.SetDestination(meshHit.position);

                meshAgent.speed = speed;

                break;

            case SwitchMachineStates.DEAD:
                m_CurrentState = currentState;
                if (!dead)
                {
                    cameraMode = true;
                    camera.SetActive(true);
                    GameManager.Instance.EnemyBecomesCamera();  
                }
                
                
                break;
            default:
                break;
        }
    }
    

    private void ExitState()
    {
        switch (m_CurrentState)
        {
            case SwitchMachineStates.IDLE:

                break;

            case SwitchMachineStates.CHASE:

                break;

            case SwitchMachineStates.ATTACK:

                break;

            case SwitchMachineStates.PATROL:

                break;



            default:
                break;
        }
    }

    private void UpdateState()
    {
        //Debug.Log(m_CurrentState);
        //Debug.Log(m_meshAgent.destination);
        switch (m_CurrentState)
        {
            case SwitchMachineStates.IDLE:
                //Comprovar si ha entrat algu al range
                if (DetectWithOverlapSphere().Length > 0)
                {
                    detect = true;
                    target = DetectWithOverlapSphere()[0].transform.position;
                }
                
                if (detect)
                {
                    ChangeState(SwitchMachineStates.CHASE);
                }
                else
                {
                   
                    StartCoroutine(patrolWait());
                }

                break;

            case SwitchMachineStates.CHASE:

                //Comprovar orientacio de moviment
          
                //Debug.Log(m_orientation);
                
                InitState(SwitchMachineStates.CHASE);
                
                if (DetectWithOverlapSphere().Length == 0)
                {
                    detect = false;
                }
                    if (meshAgent.remainingDistance <= attackRange && detect)
                    {
                        ChangeState(SwitchMachineStates.ATTACK);
                    }

                    if (!detect)
                    {
                        ChangeState(SwitchMachineStates.IDLE);

                    }


                break;
            case SwitchMachineStates.ATTACK:
                DetectWithOverlapSphere();
                
                //Debug.Log(meshAgent.remainingDistance);
                target = DetectWithOverlapSphere()[0].transform.position;
                meshAgent.SetDestination(target);
              
                if (canShoot)
                {
                    EnemyShoot();
                }
                if (meshAgent.remainingDistance < attackRange && detect)
                {
                    
                    meshAgent.speed = 0;
                }
                if (meshAgent.remainingDistance > attackRange && detect)
                {
                    ChangeState(SwitchMachineStates.CHASE);
                    
                }
                
                break;
            case SwitchMachineStates.PATROL:
                //Comprovar orientacio de moviment
                
                
                if (DetectWithOverlapSphere().Length > 0)
                {
                    detect = true;
                    target = DetectWithOverlapSphere()[0].transform.position;
                }
                
                //Debug.Log(meshAgent.remainingDistance);
                if ((meshAgent.remainingDistance<1) && !detect)
                {
                    
                    ChangeState(SwitchMachineStates.IDLE);
                }

                if (detect)
                {
                    ChangeState(SwitchMachineStates.CHASE);
                }

                //ChangeState(SwitchMachineStates.CHASE);
                break;
            
            case SwitchMachineStates.DEAD:
                if (!dead)
                {
                    cameraRotation = new Vector3(Mathf.Clamp(Input.GetAxis("Mouse Y"),-90,90) , 0, 0);
                    transform.eulerAngles = transform.eulerAngles- new Vector3(0, Input.GetAxis("Mouse X") * -1, 0);
                    camera.transform.eulerAngles = camera.transform.eulerAngles - cameraRotation;
                }
                
                if (Input.GetKeyDown(KeyCode.F))
                {
                    cameraMode = false;
                }
                if (!cameraMode&&!dead)
                {
                    dead = true;
                    camera.SetActive(false);
                    Activate(true);
                    GameManager.Instance.EnemyDies();
                    
                    //Destroy(this.gameObject);
                }

                break;


            default:
                break;
        }
    }





    public void EndHit()
    {
        Shoot();
        ChangeState(SwitchMachineStates.IDLE);
    }

    private void Shoot()
    {
        GameObject pool = this.transform.GetChild(1).gameObject;

        for (int i = 0; i < pool.transform.childCount; i++)
        {
            Transform shoot = pool.transform.GetChild(i);
            //Active, esta en desús, utilitzar activeself
            if (!shoot.gameObject.activeSelf)
            {
                shoot.gameObject.SetActive(true);
                shoot.transform.position = transform.position;
                shoot.gameObject.GetComponent<Rigidbody2D>().velocity =
                    (meshAgent.destination - shoot.transform.position).normalized * 8;
                
                break;
            }
        }

        StartCoroutine(ShootCoolDown(3));
    }

    IEnumerator ShootCoolDown(float time)
    {
        yield return new WaitForSeconds(time);

    }


    //*************************************************MONO BEHAVIOUR FUNCTIONS**********************************************
    void Awake()
    {
        animator = GetComponent<Animator>();
        meshAgent = GetComponent<NavMeshAgent>();
        meshAgent.updateRotation = false;
        meshAgent.updateUpAxis = false;
        stats = listaSo[Random.Range(0,listaSo.Length)];
        m_Bones = m_RigRoot.GetComponentsInChildren<Rigidbody>();
        Activate(false);
        
        InitValues();
        
    }

    private void Start()
    {
        
        InitState(SwitchMachineStates.IDLE);
        
        InitValues();
    }

    void Update()
    {
        UpdateState();
    }

    private void OnDestroy()
    {
        

    }

    void InitValues()
    {

        hp = stats.hp;
        dmg = stats.dmg;
        speed = stats.speed;
        detectionRange = stats.detection_range;
        attackRange = stats.attack_range;
        //animator.runtimeAnimatorController = stats.animator;
        //this.gameObject.GetComponent<SpriteRenderer>().color = stats.colorsito;





    }



    //-------------------------------------------HP GESTION-------------------------------------------//


    public void LooseHP(float dmg)
    {
        hp -= dmg;
        if (hp <= 0)
        {
            ChangeState(SwitchMachineStates.DEAD);
            meshAgent.speed = 0;
            
        }
    }





    //----------------------------------------PLAYER DETECTION----------------------------------------//

    //SPHERE CAST PARA DETECTAR ENEMIGOS
    private Collider[] DetectWithOverlapSphere()
    {
        return Physics.OverlapSphere(transform.position, 20, playerHurtbox);
        

    }

    IEnumerator patrolWait()
    {
        if (!detect)
        {
            yield return new WaitForSeconds(3f);
            ChangeState(SwitchMachineStates.PATROL);
            
        }
        
    }

    public void EnemyShoot()
    {
        RaycastHit hit;
        //Probabilitat de error
        int random = Random.Range(0, 10);
        if (random <= 4)
        {
            //Te dió.
            Physics.Raycast(camera.transform.position,new Vector3(target.x-camera.transform.position.x, target.y-camera.transform.position.y, target.z-camera.transform.position.z), out hit, attackRange);
        }
        else
        {
            Physics.Raycast(camera.transform.position,new Vector3(target.x+Random.Range(-2,3)-camera.transform.position.x, target.y+Random.Range(0,2)-camera.transform.position.y, target.z-camera.transform.position.z), out hit, attackRange);
        }

        if (hit.collider.TryGetComponent<IDamageable>(out IDamageable target2))
        {
          target2.LooseHP(dmg);  
          GameManager.Instance.SetDebugText("Has recibido "+dmg + " de daño.");
        }

        Debug.DrawLine(camera.transform.position, hit.point, Color.magenta, 1f);
        canShoot = false;
        StartCoroutine(cooldownShoot());
    }

    IEnumerator cooldownShoot()
    {
        yield return new WaitForSeconds(2f);
        canShoot = true;

    }
    public void Activate(bool state)
    {
        foreach (Rigidbody bone in m_Bones)
            bone.isKinematic = !state;
            
    }
    
}



