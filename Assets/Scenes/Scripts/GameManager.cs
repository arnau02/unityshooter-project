using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.PlayerLoop;

public class GameManager : MonoBehaviour
{
    public static GameManager Instance = null;
    [Header("Gestio de canvas")]
    [Space]
    [SerializeField] private GameObject playerCanvas;
    
    [Header("Gestio de canvas")]
    [Space]
    [SerializeField] private PlayerController playerController;
    
    [Header("Camara de jugador")] 
    [Space] 
    [SerializeField]
    private GameObject playerCamera;

    [Header("Vida")] [Space] [SerializeField]
    private TMP_Text textVida;
    [Header("TextDebugs")] [Space] [SerializeField]
    private GameObject textDebugs;
    
    private void Awake()
    {
   
        if (Instance == null)
        {
            Instance = this;
        }
        else if (Instance != this)
        {
            Destroy(gameObject);
        }

        DontDestroyOnLoad(this);
    }


    public void SetActiveCameraPlayer()
    {
        playerCamera.SetActive(!playerCamera.activeSelf);
    }

    public void SetCameraPlayer(GameObject go)
    {
        playerCamera = go;
    }
    
    public void SetActivePlayerController(bool b)
    {
        playerController.enabled=b;
    }

    public void SetPlayerController(PlayerController pl)
    {
        playerController = pl;
    }
    
    public PlayerController GetPlayerController()
    {
        return playerController;
    }

    public void EnemyBecomesCamera()
    {
        SetActiveCameraPlayer();
        SetActivePlayerCanvas();
        SetActivePlayerController(false);
    }

    public void SetActivePlayerCanvas()
    {
        playerCanvas.SetActive(!playerCanvas.activeSelf);
    }

    public void EnemyDies()
    {
        SetActiveCameraPlayer();
        SetActivePlayerCanvas();
        SetActivePlayerController(true);
    }

    public void SetTextVida(String vida)
    {
        textVida.text = vida;
    }

    public void SetDebugText(String text)
    {
        textDebugs.SetActive(true);
        textDebugs.GetComponent<TMP_Text>().text = text;
        StartCoroutine(activarTexto());
    }

    IEnumerator activarTexto()
    {
        
        yield return new WaitForSeconds(2f);
        textDebugs.SetActive(false);
    }

}
